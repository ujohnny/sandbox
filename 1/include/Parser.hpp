#ifndef PARSER_HPP
#define PARSER_HPP

#include <istream>

#include "CommonDefs.hpp"

class Parser
{
public:

  static I::TaskList parse(char **args);
  static I::TaskList parse(std::istream& stream);

private:

  static I::Task parse_internal();
};

#endif // PARSER_HPP
