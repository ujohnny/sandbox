#ifndef COMMONDEFS_HPP
#define COMMONDEFS_HPP

#include <vector>
#include <istream>
#include <ostream>
#include <stdexcept>

namespace I 
{
  static const char * const ERR_INCORRECT_INPUT = "Incorrect input sequence";
  
  struct Task
  {
    int a;
    int b;
    int c;

    friend std::istream& operator>>(std::istream& in, Task& task)
    {
      in >> task.a;
      bool isEnd = in.eof() && in.fail();
      
      in >> task.b;
      in >> task.c;
      
      // we can skip error check for b, because it will also happen for c
      if (!isEnd && in.eof() && in.fail())
      {
        throw std::invalid_argument(I::ERR_INCORRECT_INPUT);
      }

      return in;
    } 

    friend std::ostream& operator<<(std::ostream& out, const Task& task)
    {
      out << task.a << ' ' << task.b << ' ' << task.c;
      return out;
    }
  };
  
  typedef std::vector< Task > TaskList;
}

#endif // COMMONDEFS_HPP
