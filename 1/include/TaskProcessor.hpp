#ifndef TASKPROCESSOR_HPP
#define TASKPROCESSOR_HPP

#include <vector> 

namespace I
{
  class Task;
}

class TaskProcessor
{
public:
  
  typedef std::vector< double > Result;
  
  static Result process(const I::Task& task);
};

#endif // TASKPROCESSOR_HPP
