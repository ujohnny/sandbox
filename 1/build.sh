#!/bin/bash

declare clean=0
declare testing=0
declare flavor=Release

while test $# -ge 1; do
    case $1 in
        -c)
            clean=1
            ;;
        -f)
            if !( test $2 == "Debug" || test $2 == "Release" ); then
                echo "Only Debug and Release flavors are supported"
                exit 254
            fi
                
            flavor=$2
            shift
            ;;
        -h)
            echo -e "build.sh wrapper tool for build process"
            echo -e "Usage: ./build.sh [-c] [-f FLAVOR] [-h]"
            echo -e "\t-c\tPerform clean build"
            echo -e "\t-f\tSelect build flavor. Supported ones: Debug, Release (default)"
            echo -e "\t-t\tBuild and run tests"
            echo -e "\t-h\tPrints this help message"
            exit 0
            ;;
        -t)
            testing=1
            ;;
        *)
            echo "unsupported option $1"
            exit 255
            ;;
    esac
    shift
done

declare build_dir="build_$flavor"
declare output_dir='output'

if test $clean -eq 1 ; then
    for dir in $build_dir $output_dir; do
        echo "Cleaning up $dir"
        rm -rf $dir
    done
fi

mkdir -p $build_dir
rc=$?

if test $rc -ne 0; then
    echo "Creating build directory $build_dir command failed with $rc"
    exit $rc
fi

mkdir -p $output_dir
rc=$?

if test $rc -ne 0; then
    echo "Creating build directory $build_dir command failed with $rc"
    exit $rc
fi

cd $build_dir

cmake .. -DCMAKE_BUILD_TYPE=$flavor -DBUILD_TESTS=$testing
rc=$?

if test $rc -ne 0; then
    echo "CMake command failed with $rc"
    exit $rc
fi

make
rc=$?

if test $rc -ne 0; then
    echo "Build failed with $rc"
    exit $rc
fi

if test $testing -eq 1; then
    make test
    rc=$?
    if test $rc -ne 0; then
        echo "Testing failed with $rc"
        exit 253
    fi
    exit 0
fi

cd - > /dev/null

#find $build_dir -maxdepth 1 -type f -executable -exec mv {} $output_dir \;
mv $build_dir/exec $output_dir

echo "================================================================================"
echo "Build artifacts are available in "
echo "    " $(readlink -f $output_dir)
echo "================================================================================"
exit 0
