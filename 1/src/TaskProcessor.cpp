#include <cmath>

#include <CommonDefs.hpp>
#include <TaskProcessor.hpp>

TaskProcessor::Result TaskProcessor::process(const I::Task& task)
{
  TaskProcessor::Result res;
  
  if ( task.a == 0 )
  {
    res.push_back( -task.c / static_cast<double>(task.b) );
  } 
  else if ( task.c == 0 ) 
  {
    res.push_back(0);
    res.push_back( -task.b / static_cast<double>(task.a) );
  }
  else
  {
    double discriminant( task.b * task.b - 4 * task.a * task.c );
    
    if ( discriminant < 0 )
    {
      return res;
    }
    else if ( discriminant == 0 )
    {
      double root( -task.b  / 2 / task.a );
      res.push_back( root );
    }
    else
    {
      double sqrtDiscriminant( (task.a == std::abs(task.c)) ? 2 * task.a
                                                            : sqrt(discriminant) );
      
      double denominator = 2 / task.a;
      double root1 = ( -task.b - sqrtDiscriminant ) / denominator;
      double root2 = ( task.b == 0 ) ? -root1 : ( -task.b + sqrtDiscriminant ) / denominator;

      res.push_back( root1 );
      res.push_back( root2 );
    }
  }

  return res;
}
