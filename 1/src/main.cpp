#include <iostream>
#include <stdexcept> 

#include <Parser.hpp>
#include <CommonDefs.hpp>
#include <TaskProcessor.hpp>

int main(int argc, char **argv)
{
  I::TaskList tasks;
  
  try 
  {
    tasks = (argc == 1) ? Parser::parse( std::cin ) 
                        : Parser::parse( ++argv );
  } 
  catch (const std::invalid_argument& ex) 
  {
    std::cout << ex.what() << std::endl;
    return 1;
  }

  for (I::TaskList::const_iterator i = tasks.begin(); i != tasks.end(); ++i)
  {
    std::cout << "Task: " << *i << '\n';

    TaskProcessor::Result roots = TaskProcessor::process( *i );
    if (roots.empty()) 
    {
      std::cout << "No real roots\n";
    }
    else 
    {
      for (TaskProcessor::Result::const_iterator ri = roots.begin(); ri != roots.end(); ++ri)
      {
        std::cout << "Root: " << *ri << '\n';
      }
    }

    std::cout << std::endl;
  }

  return 0;
}
