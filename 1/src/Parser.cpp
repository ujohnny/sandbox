#include <istream>
#include <sstream>
#include <stdexcept>

#include <Parser.hpp>

I::TaskList Parser::parse(char **args)
{
  std::stringstream ss;

  while (args && *args) 
  {
    ss << *args << ' ';
    ++args;
  }

  return parse(ss);
}

I::TaskList Parser::parse(std::istream& stream)
{
  I::TaskList res;
  I::Task t;

  while (stream >> t)
  {
    res.push_back(t);
  }

  if (!stream.eof() && stream.fail())
  {
    throw std::invalid_argument(I::ERR_INCORRECT_INPUT);
  }
  
  return res;
}
