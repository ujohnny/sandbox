#include <sstream>
#include <cassert>

#include <Parser.hpp> 

int main()
{
  std::stringstream ss;
  ss << "1 2 3";

  I::TaskList tl( Parser::parse(ss) );
  assert(tl.size() == 1);
  assert(tl[0].a == 1);
  assert(tl[0].b == 2);
  assert(tl[0].c == 3);
}
