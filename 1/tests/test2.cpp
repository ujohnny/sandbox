#include <sstream>
#include <cassert>

#include <Parser.hpp> 
#include <iostream>
int main()
{
  char **s = new char*[4];
  char **t = s;
  *t = new char(49); ++t;
  *t = new char(50); ++t;
  *t = new char(51); ++t;
  *t = new char(0);


  I::TaskList tl( Parser::parse(s) );
  assert(tl.size() == 1);
  assert(tl[0].a == 1);
  assert(tl[0].b == 2);
  assert(tl[0].c == 3);
}
